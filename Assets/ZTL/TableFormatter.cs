﻿using System.Collections.Generic;
using ZeroFormatter;
using ZeroFormatter.Formatters;
using ZeroFormatter.Internal;

namespace ZF
{
	internal class TableFormatter<TTypeResolver, TKey, TValue> : Formatter<TTypeResolver, Table<TKey, TValue>>
		where TTypeResolver : ITypeResolver, new()
		where TValue : class, IRow
	{
		public override int? GetLength()
		{
			return null;
		}

		public override int Serialize(ref byte[] bytes, int offset, Table<TKey, TValue> value)
		{
			// use sequence format.
			if (value == null)
			{
				BinaryUtil.WriteInt32(ref bytes, offset, -1);
				return 4;
			}

			var startOffset = offset;

			// dictionary
			offset += BinaryUtil.WriteInt32(ref bytes, offset, value.indexedData.Count);

			var keyPairFormatter = Formatter<TTypeResolver, KeyValuePair<TKey, TValue>>.Default;
			foreach (var item in value.indexedData)
			{
				offset += keyPairFormatter.Serialize(ref bytes, offset, new KeyValuePair<TKey, TValue>(item.Key, item.Value));
			}

			return offset - startOffset;
		}

		public override Table<TKey, TValue> Deserialize(ref byte[] bytes, int offset, DirtyTracker tracker, out int byteSize)
		{
			byteSize = 4;

			// dictionary
			var dictionaryLength = BinaryUtil.ReadInt32(ref bytes, offset);
			if (dictionaryLength == -1)
				return null;

			var keyPairFormatter = Formatter<TTypeResolver, KeyValuePair<TKey, TValue>>.Default;

			Table<TKey, TValue> table = new Table<TKey, TValue>();
			table.indexedData = new Dictionary<TKey, TValue>();

			offset += 4;
			for (int i = 0; i < dictionaryLength; i++)
			{
				var val = keyPairFormatter.Deserialize(ref bytes, offset, tracker, out int size);
				table.indexedData.Add(val.Key, val.Value);
				offset += size;
			}

			return table;
		}
	}
}