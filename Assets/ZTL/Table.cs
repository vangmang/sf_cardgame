﻿using System.Collections;
using System.Collections.Generic;
using ZeroFormatter;

#if INCLUDE_ONLY_CODE_GENERATION == false
namespace ZF
{
	public interface ITable {}

	[ZeroFormattable]
	public class Table<TKey, TValue> : IEnumerable<TValue>, ITable
		where TValue : class, IRow
	{
		[Index(0)]
		public virtual Dictionary<TKey, TValue> indexedData { get; internal set; }

		[IgnoreFormat]
		public virtual List<TValue> dataList { get; internal set; }

		[IgnoreFormat]
		public TValue this[TKey index] => indexedData[index];

		public IEnumerator<TValue> GetEnumerator() => dataList.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public TValue Get(TKey key)
		{
			if (indexedData.ContainsKey(key))
				return indexedData[key];
			return null;
		}
	}
}
#endif