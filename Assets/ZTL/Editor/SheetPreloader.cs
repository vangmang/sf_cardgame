using System;
using System.Text;
using System.Threading.Tasks;
using Sirenix.OdinInspector;
using Sirenix.Utilities.Editor;
using UnityEngine;
using ZTL.Editor.GDrive;

namespace ZTL.Editor
{
    [Serializable]
    public class SheetPreloader
    {
        public enum ImportModeType
        {
            Local,
            GoogleDrive,
        }
        
        private bool IsLocalMode() =>  importMode == ImportModeType.Local;
        private bool IsGDriveMode() => importMode == ImportModeType.GoogleDrive;

        [EnumToggleButtons, GUIColor(0.8f, 0.9f, 1)]
        [LabelText("Import 방법")]
        public ImportModeType importMode = ImportModeType.Local;

        [Header("Excel 파일 경로")]
        [FolderPath(ParentFolder = "@ZTLEditor.ZTL_EDITOR_PATH_FROM_ASSETS")]
        [ShowIf("IsLocalMode"), BoxGroup("로컬 임포터")]
        [LabelText("@ZTLEditor.ZTL_EDITOR_PATH_FROM_ASSETS")] [InlineButton("OpenPath", "탐색기로 열기")]
        public string localExcelPath = "@ExcelSheets";
        
        [Header("CSV 다운로드 경로")]
        [FolderPath(ParentFolder = "@ZTLEditor.ZTL_EDITOR_PATH_FROM_ASSETS")]
        [ShowIf("IsGDriveMode"), BoxGroup("Google Drive 임포터")]
        [LabelText("@ZTLEditor.ZTL_EDITOR_PATH_FROM_ASSETS")] [InlineButton("OpenPath", "탐색기로 열기")] 
        public string csvDownloadPath = "@GoogleDriveSheets";
        
        private void OpenPath()
        {
            ZTLEditor.OpenExcelDirectory();
        }

        [Header("CSV 다운로드 경로")] [LabelText("Google Drive 문서 정보")]
        [ShowIf("IsGDriveMode"), BoxGroup("Google Drive 임포터")]
        public GoogleSpreadSheetInput[] sheetDownloadTargets;

        [LabelText("Export시 자동 다운로드")]
        [ShowIf("IsGDriveMode"), BoxGroup("Google Drive 임포터")]
        public bool isAutoDownloadAtExport = true;
        
        private string GetDownloadButtonText()
        {
            GUIHelper.RequestRepaint();
            if (isDownloadProcessing)
            {
                if (GoogleAPIConnector.isLogin)
                    return "다운로드 중...";
                return "Google Api 인증중...";
            }
            return "Google Drive에서 CSV 다운로드";
        }

        [Button("@GetDownloadButtonText()", ButtonSizes.Large), GUIColor(1, 0.8f, 0.8f)]
        [Indent(5)]
        [ShowIf("IsGDriveMode"), BoxGroup("Google Drive 임포터")]
        [EnableIf("IsDownloadButtonEnable")]
#pragma warning disable 4014
        public void DownloadSheetsFromGoogleDrive() => DownloadSheetsFromGoogleDriveAsync();
#pragma warning restore 4014

        public async Task DownloadSheetsFromGoogleDriveAsync()
        {
            if (isDownloadProcessing == false)
                await DownloadProcess();
        }

        // ReSharper disable NotAccessedField.Local NotAccessedField.Global
        [ProgressBar(0, "_totalDownloadFileCount", Segmented = true)]
        [ShowIf("@IsGDriveMode() && isDownloadProcessing && GoogleAPIConnector.isLogin")]
        [BoxGroup("Google Drive 임포터")]
        [LabelText("다운로드 진행률"), ReadOnly, SerializeField]
        private float downloadProgressValue1;
        private int _totalDownloadFileCount;
        // ReSharper restore NotAccessedField.Local NotAccessedField.Global
        
        // unity serializer 사용하기 위한 string 사용 (long type 미지원)
        [SerializeField, HideInInspector] private string lastUpdatedTimeTickStr = "0";
        [LabelText("최근 다운로드 일자")] [ShowIf("IsGDriveMode"), BoxGroup("Google Drive 임포터")]
        [ReadOnly, ShowInInspector]
        public string lastUpdatedTimeLabel => ZTLUtils.GetTimeElapsedString(new DateTime(long.Parse(lastUpdatedTimeTickStr)));

        [NonSerialized] 
        public bool isDownloadProcessing;

        private bool IsDownloadButtonEnable()
        {
            return !isDownloadProcessing && sheetDownloadTargets?.Length > 0;
        }
        
        public string GetSheetsRelativeDirectory()
        {
            switch (importMode)
            {
                case ImportModeType.Local:
                    return localExcelPath;
                case ImportModeType.GoogleDrive:
                    return csvDownloadPath;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public async Task DownloadProcess()
        {
            isDownloadProcessing = true;
            downloadProgressValue1 = 1;
            try
            {
                var downloader = new GoogleDriveCsvDownloader();

                Debug.LogWarning($"[{nameof(SheetPreloader)}] 로그인을 시도하고 있습니다. 잠시만 기다려주세요.");
                await downloader.Login();
                var sheets =
                    await downloader.CollectSheetMetaInfos(sheetDownloadTargets);

                foreach (var sheet in sheets)
                    Debug.Log($"[{nameof(SheetPreloader)}] {sheet.sheetName} {sheet.fileId} {sheet.gid}");
                _totalDownloadFileCount = sheets.Length;

                var sb = new StringBuilder();
                sb.AppendLine($"[{nameof(SheetPreloader)}] CSV 파일을 다운로드합니다. (시트 개수: {sheets.Length}) 잠시만 기다려주세요.");
                for (var i = 0; i < sheets.Length; i++)
                    sb.AppendLine($"{i + 1}. {sheets[i].fileName}/{sheets[i].sheetName}");
                Debug.LogWarning(sb.ToString());

                foreach (var sheet in sheets)
                {
                    Debug.Log($"[{nameof(SheetPreloader)}] Download Sheet to CSV => {sheet.sheetName}.csv");
                    await downloader.Download(sheet, ZTLEditor.sheetFileAbsoluteDirectory);
                    downloadProgressValue1 += 1;
                }

                lastUpdatedTimeTickStr = DateTime.Now.Ticks.ToString();
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
                throw;
            }
            finally
            {
                UnityEngine.Debug.Log($"[{nameof(SheetPreloader)}] CSV 다운로드 완료: {downloadProgressValue1 - 1} csv파일 생성 됨.");
                
                isDownloadProcessing = false;
            }
        }
    }
}