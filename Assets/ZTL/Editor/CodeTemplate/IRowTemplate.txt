// ZFExcelExporter에서 생성된 코드입니다.

using System.Collections.Generic;
using ZeroFormatter;
@UsingObscuredTypes

namespace ZF
{
    [ZeroFormattable]
    public partial class @ClassNameRow : IRow
    {
@Properties
    }
@ClassExtend
}