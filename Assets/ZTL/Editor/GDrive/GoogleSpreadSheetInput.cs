using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace ZTL.Editor.GDrive
{
    [Serializable]
    public class GoogleSpreadSheetInput
    {
        [SerializeField, HideInInspector] public string fileName = "*";
        [LabelText("문서 주소 입력")] [SuffixLabel("url", true)]
        [InlineButton("OpenMyUrl", "URL 열기")] [Title("@fileName")]
        public string url;
        [LabelText("무시할 시트 이름")] [SuffixLabel(", 로 시트명 구분", true)] [SerializeField] 
        private string ignoreSheetNamesRaw;

        [LabelText("읽어온 시트")] [ReadOnly] public string[] downloadedSheetList;
        
        public string[] ignoreSheetNames
        {
            get
            {
                if (string.IsNullOrEmpty(ignoreSheetNamesRaw)) 
                    return new string[0];
                
                string[] split = ignoreSheetNamesRaw.Trim().Split(',');
                List<string> tmp = new List<string>();
                foreach (var s in split)
                {
                    if (string.IsNullOrWhiteSpace(s)) continue;
                    tmp.Add(s.Trim());
                }
                return tmp.ToArray();
            }
        }

        private void OpenMyUrl()
        {
            if (string.IsNullOrWhiteSpace(url) == false)
                Application.OpenURL(url.Trim());
        }
    }
}