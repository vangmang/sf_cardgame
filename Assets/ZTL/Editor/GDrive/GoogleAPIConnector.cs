using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ZTL.Editor.GDrive
{
    public static class GoogleAPIConnector
    {
        // https://github.com/googlesamples/oauth-apps-for-windows/
        
        // connect to google api loadcomplete/DriveAutomation 
        // Unity-GDriveDownloader
        private static string ClientKey => GDriveConfig.Instance.clientId;
        private static string ClientSecretKey => GDriveConfig.Instance.clientSecretKey;
        private static string APIKey => GDriveConfig.Instance.apiKey;
        
        private const string AUTHORIZATION_ENDPOINT = "https://accounts.google.com/o/oauth2/v2/auth";
        private const string TOKEN_ENDPOINT = "https://www.googleapis.com/oauth2/v4/token";

        public static bool isLogin => GoogleLogin.cachedLoginData != null 
                                      && GoogleLogin.cachedLoginData.IsExpired() == false;

        public static async Task DoOAuth()
        {
            if (GoogleLogin.cachedLoginData != null)
            {
                if (GoogleLogin.cachedLoginData.IsExpired() == false)
                    return;
                
                await RequestRefreshToken(GoogleLogin.cachedLoginData);
                
                if (GoogleLogin.cachedLoginData.IsExpired() == false)
                    return;
            }
            
            // Generates state and PKCE values.
            string state = RandomDataBase64Url(32);
            string codeVerifier = RandomDataBase64Url(32);
            string codeChallenge = Base64UrlEncodeNoPadding(Sha256(codeVerifier));
            const string CODE_CHALLENGE_METHOD = "S256";
            
            string redirectUri = $"http://{IPAddress.Loopback}:{GetRandomUnusedPort()}/";
            
            Debug.Log($"[{nameof(GoogleAPIConnector)}] Redirect URI: {redirectUri}");
            
            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(redirectUri);
            Debug.Log($"[{nameof(GoogleAPIConnector)}] Listening..");
            http.Start();
            
            string authorizationRequest = string.Format("{0}?response_type=code&scope={6}&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}",
                AUTHORIZATION_ENDPOINT,
                Uri.EscapeDataString(redirectUri),
                ClientKey,
                state,
                codeChallenge,
                CODE_CHALLENGE_METHOD,
                BuildScopesString());
            
            var process = LaunchWebBrowserPopup(authorizationRequest);

            var context = await http.GetContextAsync();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = "login success.";
            var buffer = Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                http.Stop();
                Debug.Log($"[{nameof(GoogleAPIConnector)}] HTTP server stopped.");
                
                try
                {
                    process?.CloseMainWindow();
                }
                catch (Exception e)
                {
                    Debug.LogError(e.ToString());
                    throw;
                }
            });
            
            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null)
            {
                Debug.Log($"[{nameof(GoogleAPIConnector)}] OAuth authorization error: {context.Request.QueryString.Get("error")}.");
                return;
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null)
            {
                Debug.Log($"[{nameof(GoogleAPIConnector)}] Malformed authorization response. " + context.Request.QueryString);
                return;
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incomingState = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incomingState != state)
            {
                Debug.Log($"Received request with invalid state ({incomingState})");
                return;
            }
            Debug.Log("Authorization code: " + code);

            await PerformCodeExchange(code, codeVerifier, redirectUri);
        }

        private static async Task PerformCodeExchange(string code, string codeVerifier, string redirectUri)
        {
            Debug.Log($"[{nameof(GoogleAPIConnector)}] Exchanging code for tokens...");

            // builds the request
            string tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&code_verifier={3}&client_secret={4}&scope=&grant_type=authorization_code",
                code,
                Uri.EscapeDataString(redirectUri),
                ClientKey,
                codeVerifier,
                ClientSecretKey
            );

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(TOKEN_ENDPOINT);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(byteVersion, 0, byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
                using (var reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();
                    Debug.Log(responseText);

                    // converts to dictionary
                    var loginData = JsonConvert.DeserializeObject<GoogleLogin>(responseText);
                    loginData.ResetExpireTime(loginData.expiresIn);
                    GoogleLogin.cachedLoginData = loginData;
                }
            }
            catch (WebException ex)
            {
                if (ex.Status != WebExceptionStatus.ProtocolError) return;
                if (ex.Response is HttpWebResponse response)
                {
                    Debug.LogError($"[{nameof(GoogleAPIConnector)}] HTTP: {response.StatusCode}");
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        // reads response body
                        string responseText = await reader.ReadToEndAsync();
                        Debug.LogError($"[{nameof(GoogleAPIConnector)}] {responseText}");
                    }
                }
            }
        }

        private static async Task<string> RequestGet(string accessToken, string uri)
        {
            Debug.Log($"[{nameof(GoogleAPIConnector)}] Request -> {uri}");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";
            request.Headers.Add($"Authorization: Bearer {accessToken}");
            request.Accept = "application/json";

            WebResponse response = await request.GetResponseAsync();
            using (StreamReader responseReader = new StreamReader(response.GetResponseStream()))
            {
                 string responseText = await responseReader.ReadToEndAsync();
                 return responseText;
            }
        }
        
        private static async Task RequestRefreshToken(GoogleLogin loginInfo)
        {
            Debug.Log($"[{nameof(GoogleAPIConnector)}] Refresh Token {loginInfo}");
            
            string tokenRequestBody = string.Format("client_id={0}&client_secret={1}&refresh_token={2}&grant_type=refresh_token",
                ClientKey,
                ClientSecretKey,
                loginInfo.refreshToken
            );

            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(TOKEN_ENDPOINT);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(byteVersion, 0, byteVersion.Length);
            stream.Close();

            WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
            try
            {
                using (StreamReader responseReader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    string responseText = await responseReader.ReadToEndAsync();
                    JsonConvert.PopulateObject(responseText, loginInfo);
                    loginInfo.ResetExpireTime(loginInfo.expiresIn);
                    GoogleLogin.cachedLoginData = loginInfo;
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
                throw;
            }
        }

        public static async Task<string> RequestSheetInfo(string fileId)
        {
            string requestUri = $"https://sheets.googleapis.com/v4/spreadsheets/{fileId}?key={APIKey}";
            return await RequestGet(GoogleLogin.cachedLoginData.accessToken, requestUri);
        }
        
        public static async Task<string> RequestSheetCsvDownload(string fileId, int gid = 0)
        {
            string requestUri = $"https://docs.google.com/spreadsheets/d/{fileId}/export?format=csv&gid={gid}";
            return await RequestGet(GoogleLogin.cachedLoginData.accessToken, requestUri);
        }
        
        private static string BuildScopesString()
        {
            // scope 리스트는 google api에서 설정
            // 사용할 scope를 아래에 배열로 추가
            string[] accessScopes = {
                "https://www.googleapis.com/auth/spreadsheets",
            };
            StringBuilder sb = new StringBuilder();
            foreach (var accessScope in accessScopes) 
                sb.Append($"{accessScope} ");
            return sb.ToString().TrimEnd();
        }

        // ref http://stackoverflow.com/a/3978040
        private static int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }

        /// <summary>
        /// Returns URI-safe data with a given input length.
        /// </summary>
        /// <param name="length">Input length (nb. output will be longer)</param>
        /// <returns></returns>
        private static string RandomDataBase64Url(uint length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return Base64UrlEncodeNoPadding(bytes);
        }

        /// <summary>
        /// Returns the SHA256 hash of the input string.
        /// </summary>
        /// <param name="inputStirng"></param>
        /// <returns></returns>
        private static byte[] Sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        /// <summary>
        /// Base64url no-padding encodes the given input buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static string Base64UrlEncodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }


        private static Process LaunchWebBrowserPopup(string url)
        {
            try
            {
                return Process.Start("chrome", url + " --new-window");
            }
            catch
            {
                try
                {
                    return Process.Start("firefox", "-new-window " + url);
                }
                catch
                {
                    try
                    {
                        return Process.Start("msedge", url + " --new-window");
                    }
                    catch
                    {
                        Process.Start(url);
                        return null;
                    }
                }
            }           
        }
    }
}