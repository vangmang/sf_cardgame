using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ZTL.Editor.GDrive
{
    public class GoogleDriveCsvDownloader
    {
        public async Task Login()
        {
            await GoogleAPIConnector.DoOAuth();
        }

        public async Task<GoogleSpreadSheetMeta[]> CollectSheetMetaInfos(GoogleSpreadSheetInput[] inputList)
        {
            // 파일 url만 등록하여 fileId Parsing https://docs.microsoft.com/en-us/dotnet/api/system.uri.segments?view=net-5.0
            
            var metaList = new List<GoogleSpreadSheetMeta>();

            foreach (var input in inputList)
            {
                string fileId = ZTLUtils.ParseFileIdFromUri(input.url);
                var json = await GoogleAPIConnector.RequestSheetInfo(fileId);

                List<string> loadedSheetList = new List<string>();
                
                var sheetInfo = JsonConvert.DeserializeObject<GoogleSpreadSheetJson>(json);
                // collect sheets
                foreach (var sheet in sheetInfo.sheets)
                {
                    if (input.ignoreSheetNames.Contains(sheet.properties.title, StringComparer.OrdinalIgnoreCase))
                        continue;
                    
                    // sheet ignore token
                    if (sheet.properties.title[0] == '#')
                        continue;

                    foreach (var meta in metaList)
                    {
                        if (meta.sheetName.Equals(sheet.properties.title, StringComparison.OrdinalIgnoreCase))
                            goto NextLoop;
                    }

                    metaList.Add(new GoogleSpreadSheetMeta
                    {
                        fileName = sheetInfo.properties.title,
                        fileId = fileId,
                        gid = sheet.properties.sheetId,
                        sheetName = sheet.properties.title
                    });
                    
                    loadedSheetList.Add(sheet.properties.title);
                    Debug.Log($"[{nameof(GoogleDriveCsvDownloader)}] Add Sheet {sheetInfo.properties.title}.{sheet.properties.title}");
                    NextLoop: ;
                }
                input.fileName = sheetInfo.properties.title;
                input.downloadedSheetList = loadedSheetList.ToArray();
            }
            return metaList.ToArray();
        }

        public async Task Download(GoogleSpreadSheetMeta downloadTarget, string absoluteDirectory)
        {
            Debug.Log($"[{nameof(GoogleDriveCsvDownloader)}] Start Download => {downloadTarget.sheetName}.csv");
            var requestResult = 
                await GoogleAPIConnector.RequestSheetCsvDownload(downloadTarget.fileId, downloadTarget.gid);

            var downloadFilePath = Path.Combine(absoluteDirectory, $"{downloadTarget.sheetName}.csv");

            if (Directory.Exists(absoluteDirectory) == false)
                Directory.CreateDirectory(absoluteDirectory);
            
            if (File.Exists(downloadFilePath))
            {
                File.Delete(downloadFilePath);
                Debug.Log($"[{nameof(GoogleDriveCsvDownloader)}] {downloadTarget.sheetName}.csv Remove for update file");
            }
            
            using (var fs = new FileStream(downloadFilePath, 
                FileMode.CreateNew))
            using (var sw = new StreamWriter(fs, Encoding.UTF8))
            {
                Debug.Log($"[{nameof(GoogleDriveCsvDownloader)}] Write {downloadTarget.sheetName}.csv");
                await sw.WriteAsync(requestResult);
            }
        }
    }
}