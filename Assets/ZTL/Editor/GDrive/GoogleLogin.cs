﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnityEditor;

namespace ZTL.Editor.GDrive
{
    [Serializable]
    public class GoogleLogin
    { 
        [JsonProperty("access_token")]
        public string accessToken;
        
        [JsonProperty("expires_in")]
        public int expiresIn; // 토큰 유효 시간 (초)
        
        [JsonProperty("refresh_token")]
        public string refreshToken;
        
        [JsonProperty("scope")]
        public string scope; // 접근 권한
        
        [JsonProperty("token_type")]
        public string tokenType; // 일반적으로 Bearer
        
        [JsonProperty] public DateTime loginTime;
        [JsonProperty] public DateTime expireTime;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{nameof(accessToken)}: {accessToken}");
            sb.AppendLine($"{nameof(expiresIn)}: {expiresIn}");
            sb.AppendLine($"{nameof(refreshToken)}: {refreshToken}");
            sb.AppendLine($"{nameof(scope)}: {scope}");
            sb.AppendLine($"{nameof(tokenType)}: {tokenType}");
            sb.AppendLine($"{nameof(loginTime)}: {loginTime}");
            sb.AppendLine($"{nameof(expireTime)}: {expireTime}");
            return sb.ToString();
        }

        public void ResetExpireTime(int seconds)
        {
            loginTime  = DateTime.Now;
            expireTime = DateTime.Now + TimeSpan.FromSeconds((long)(seconds * 0.8f));
            Debug.Log($"[{nameof(GoogleLogin)}] {nameof(ResetExpireTime)} Reset Expire Time => {expireTime}");
        }
        
        
        public bool IsExpired() => DateTime.Now >= expireTime;
        
        private static GoogleLogin cachedLoginDataInternal;
        
        public static GoogleLogin cachedLoginData
        {
            get
            {
                if (File.Exists(cachedDataFilePath) == false) return null;

                var sb = new StringBuilder();
                using (var fs = new FileStream(cachedDataFilePath, FileMode.OpenOrCreate))
                using (var sr = new StreamReader(fs))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null) 
                        sb.AppendLine(line);
                }
                cachedLoginDataInternal = JsonConvert.DeserializeObject<GoogleLogin>(sb.ToString());
                return cachedLoginDataInternal;
            }
            set
            {
                if (File.Exists(cachedDataFilePath)) 
                    File.Delete(cachedDataFilePath);
                
                cachedLoginDataInternal = value;
                var json = JsonConvert.SerializeObject(value);
                using (var fs = new FileStream(cachedDataFilePath, FileMode.OpenOrCreate))
                using (var sw = new StreamWriter(fs))
                    sw.Write(json);
            }
        }

        private static string cachedDataFilePath
        {
            get
            {
                const string LOCAL_CACHED_LOGIN_DATA_FILE_NAME = "cachedAuth.tmp";
                
                string cacheDataPath =
                    Path.Combine(Path.GetDirectoryName(AssetDatabase.GetAssetPath(ZTLConfig.Instance)),
                        LOCAL_CACHED_LOGIN_DATA_FILE_NAME);
                return cacheDataPath;
            }
        }
    }
}