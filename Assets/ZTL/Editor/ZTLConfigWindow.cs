using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace ZTL.Editor
{
    public class ZTLConfigWindow : OdinEditorWindow
    {
        [OnInspectorGUI, PropertyOrder(-1)]
        private void DrawLogo()
        {
            var logoTexture = AssetDatabase.LoadAssetAtPath<Texture>(@"Assets/ZTL/Editor/logo.png");
            if (logoTexture == null) return;
            GUILayout.BeginVertical();
            GUILayout.Label(logoTexture);
            GUILayout.EndVertical();
        }
    
        [MenuItem("Tools/ZTL/Open Config... &F8")]
        public static void Init()
        {
            var window = GetWindow<ZTLConfigWindow>("ZTL Config", true);
            window.Config = ZTLConfig.Instance;
            window.minSize = new Vector2(540, 0);
        }
    
        [InlineEditor(InlineEditorModes.FullEditor, InlineEditorObjectFieldModes.Foldout, Expanded = true)]
        // ReSharper disable once NotAccessedField.Global
        public ZTLConfig Config;
    
        [DetailedInfoBox("ZTL v2.0.3 : Zero Table Loader\n사용시 주의사항: ...",
@"ZTL v2.0.3 : Zero Table Loader
사용시 주의사항:
- 첫 실행이나 초기화 후 실행시 zfc.exe가 실행이 안 되는 문제가 있습니다. 이 경우 다시 Export를 해주시면 zfc.exe가 실행되며 지정한 Script Path에 _ZeroFormatterCodeGenerated.cs가 포함 되어있는 것을 확인할 수 있습니다.

- ZTL은 엑셀파일이 변경 되었을 경우 자동으로 클래스코드 및 바이너리 파일을 생성해주는 기능이 없습니다. 반드시 변경사항을 최신화 시키고 싶을 경우 Export를 수동으로 눌러줘야 합니다. (비밀번호 변경시에도 마찬가지)

자세한 사용법은 아래에서 확인할 수 있습니다."
        )]
        [Button("문서 열기"), PropertySpace(15)]
        public void OpenDocumentation()
        {
            Application.OpenURL(@"https://gitlab.loadcomplete.com/bk/ZTL/blob/master/README.md");
        }
        
        [Button("바이너리 내보내기", ButtonSizes.Large), GUIColor(1, 0.6f, 0.5f), PropertySpace(20)]
        public void Export()
        {
            ZTLEditor.Export();
        }
        
        [Button("닫기", ButtonSizes.Large), GUIColor(0.4f, 0.8f, 1), PropertySpace(10)]
        public void CloseWindow()
        {
            Close();
        }
    }
}