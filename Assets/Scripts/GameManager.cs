﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get; private set; }

    public ShipPlayer playerShip;
    public ShipEnemy enemyShip;

    [SerializeField] UIItemExitBtn itemExitBtn;


    void Awake()
    {
        instance = this;
    }

    public bool isMyTurn;

    public void ExitTurn()
    {
        isMyTurn = false;
        itemExitBtn.SetDim(true);
        playerShip.ClearCards();
        enemyShip.PlayAI();
    }

    public void ToMyTurn()
    {
        playerShip.UpdateInfo();
        playerShip.ShowCards(() =>
        {
            isMyTurn = true;
            itemExitBtn.SetDim(false);
        });
    }

    public void OnClick_ExitTurn()
    {
        ExitTurn();
    }
}
