﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZF;

public static class DataManager
{
    public static WeaponDataTable weaponData { get; }
    public static ShieldDataTable shieldData { get; }
    public static EngineDataTable engineData { get; }
    public static HullDataTable hullData { get; }

    static DataManager()
    {
        weaponData = WeaponDataTable.LoadBinary();
        shieldData = ShieldDataTable.LoadBinary();
        engineData = EngineDataTable.LoadBinary();
        hullData = HullDataTable.LoadBinary();
    }

    public static WeaponDataRow GetWeaponData(int key)
    {
        return weaponData.dataList[key];
    }

    public static ShieldDataRow GetShieldData(int key)
    {
        return shieldData.dataList[key];
    }

    public static EngineDataRow GetEngineData(int key)
    {
        return engineData.dataList[key];
    }

    public static HullDataRow GetHullData(int key)
    {
        return hullData.dataList[key];
    }
}
