﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Linq;

public class UICardDeck : MonoBehaviour
{
    private int registeredCount;

    [SerializeField] List<UIItemCard> cardList;
    [SerializeField] bool visible;

    public void RegisterCard(Action<UIItemCard, bool> callback_setInfo)
    {
        //Debug.Log(registeredCount);
        callback_setInfo?.Invoke(cardList[registeredCount], visible);
        registeredCount++;
    }

    Coroutine coroutine;
    public void ShowCards(Action callback)
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoShowCards(callback));
    }

    public void ClearCards()
    {
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoClearCards());
    }

    private IEnumerator CoShowCards(Action callback)
    {
        float max = 3f;
        float t = max / cardList.Count;

        foreach (var card in cardList)
        {
            card.getRtBody.localScale = Vector3.zero;
            card.getRtBody.anchoredPosition = new Vector2(1062.3f, 59.8f);
            card.getRtBody.localRotation = Quaternion.Euler(0f, 0f, 58.451f);

            card.getRtBody.DOAnchorPos(card.originPos, t).SetEase(Ease.InOutSine);
            card.getRtBody.DOLocalRotate(Vector3.zero, t).SetEase(Ease.InOutSine);
            card.getRtBody.DOScale(Vector3.one, t).SetEase(Ease.InOutSine);
            yield return new WaitForSeconds(t * 0.25f);
        }
        yield return new WaitForSeconds(t);
        callback?.Invoke();
        SetCardRaycast(true);
    }

    // 카드가 왼쪽으로 사라지는 연출
    private IEnumerator CoClearCards()
    {
        //List list = new System.Collections.Generic.List();
        SetCardRaycast(false);
        float max = 3;
        float t = max / cardList.Count;
        for(int i = cardList.Count - 1; i >= 0; i--)
        {
            cardList[i].getRtBody.DOAnchorPosX(-1280f, t).SetEase(Ease.InOutSine);
            yield return new WaitForSeconds(t * 0.25f);
        }
        yield return new WaitForSeconds(t);
    }

    private void SetCardRaycast(bool enable)
    {
        cardList.ForEach(card =>
        {
            card.SetRaycast(enable);
        });
    }
}
