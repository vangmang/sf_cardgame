﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZF;

public class ModuleEngine : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;
    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Engine)
                  .SetParentModule(this);

        for (int i = 0; i < DataManager.engineData.dataList.Count; i++)
        {
            cardDeck.RegisterCard((card, visible) =>
            {
                var engineData = DataManager.GetEngineData(i);
                card.SetCardIndex(i)
                    .SetNameText(engineData.name)
                    .SetTargetRequire(engineData.targetRequire)
                    .SetDescriptionText(engineData.description)
                    .SetPowerUsageText(engineData.powerUsage)
                    .SetTypeImage(ModuleType.Engine)
                    .SetIconImage(AssetManager.instance.GetEngineSprite(i))
                    .SetActionPointerDown(SetActionPointerDown)
                    .SetActionEndDrag(EndDrag)
                    .SetInteraction(visible);
            });
        }
    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        var engineData = DataManager.GetEngineData(itemCard.cardIndex);
        return UseEngine(itemCard, engineData);
    }

    public bool UseEngine(UIItemCard itemCard, EngineDataRow engineData)
    {
        bool canUse = parentShip.power - engineData.powerUsage >= 0;
        if (canUse)
        {
            switch (engineData.id)
            {
                case 1: // 동력 공급
                    if (parentShip.power >= parentShip.maxPower)
                        return false;
                    else if (!isCardAboutToUse)
                        return false;
                    else
                    {
                        UseCard(itemCard, () =>
                        {
                            parentShip.UsePower(engineData.powerUsage);
                            parentShip.RecoveryPower(2);
                        });
                        return true;
                    }
                case 2:
                    if (parentShip.power >= parentShip.maxPower)
                        return false;
                    else if (!isCardAboutToUse)
                        return false;
                    else
                    {

                        UseCard(itemCard, () =>
                        {
                            parentShip.SetOverload(true);
                            parentShip.UsePower(engineData.powerUsage);
                            parentShip.RecoveryPower(5);
                        });
                        return true;
                    }
                default:
                    return false;
            }
        }
        else
            return false;
    }
}
