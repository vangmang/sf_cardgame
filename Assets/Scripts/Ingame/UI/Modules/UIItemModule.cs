﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIItemModule : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public static ModuleBase selectedModule { get; private set; }
    
    [SerializeField] RectTransform rtBody;
    [SerializeField] Text txtModuleType, txtHitpoints;
    [SerializeField] Image imgHitpoints, imgBg;
    [SerializeField] GameObject goIndicator;
    public RectTransform getRtBody => rtBody;

    ModuleBase parentModule;
    public UIItemModule SetParentModule(ModuleBase parentModule)
    {
        this.parentModule = parentModule;
        return this;
    }
    public UIItemModule SetModuleTypeText(ModuleType type)
    {
        switch (type)
        {
            case ModuleType.Weapon: 
                txtModuleType.text = "무기";
                break;
            case ModuleType.Shield:
                txtModuleType.text = "쉴드";
                break;
            case ModuleType.Engine:
                txtModuleType.text = "엔진";
                break;
            case ModuleType.Hull:
                txtModuleType.text = "선체";
                break;
        }
        return this;
    }


    public void SetHitpoints(int currentHP, int maxHP)
    {
        txtHitpoints.text = $"{currentHP}/{maxHP}";
        imgHitpoints.fillAmount = (float)currentHP / maxHP;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        selectedModule = parentModule;
        goIndicator.SetActive(true);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        selectedModule = null;
        goIndicator.SetActive(false);
    }
}
