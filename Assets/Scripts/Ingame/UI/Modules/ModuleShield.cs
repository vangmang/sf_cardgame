﻿using System.Collections;
using System.Collections.Generic;
using ZF;
using UnityEngine;

public class ModuleShield : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;

    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Shield)
                  .SetParentModule(this);


        for (int i = 0; i < DataManager.shieldData.dataList.Count; i++)
        {
            cardDeck.RegisterCard((card, visible) =>
            {
                var shieldData = DataManager.GetShieldData(i);
                card.SetCardIndex(i)
                    .SetNameText(shieldData.name)
                    .SetTargetRequire(shieldData.targetRequire)
                    .SetDescriptionText(shieldData.description)
                    .SetPowerUsageText(shieldData.powerUsage)
                    .SetTypeImage(ModuleType.Shield)
                    .SetIconImage(AssetManager.instance.GetShieldSprite(i))
                    .SetActionPointerDown(SetActionPointerDown)
                    .SetActionEndDrag(EndDrag)
                    .SetInteraction(visible);
            });
        }
    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        var shieldData = DataManager.GetShieldData(itemCard.cardIndex);
        return UseShield(itemCard, shieldData);
    }

    public bool UseShield(UIItemCard itemCard, ShieldDataRow shieldData)
    {
        bool canUse = parentShip.power - shieldData.powerUsage >= 0;
        switch (shieldData.id)
        {
            case 1: // 쉴드 회복 
                var powerUsage = shieldData.powerUsage;
                if (parentShip.isShieldBatteryEnable)
                {
                    powerUsage--;
                }
                canUse = parentShip.power - powerUsage >= 0;
                if (canUse)
                {

                    if (parentShip.shield >= parentShip.maxShield)
                        return false;
                    else
                    {
                        UseCard(itemCard, () =>
                        {
                            parentShip.RecoveryShield(4);
                            parentShip.UsePower(powerUsage);
                        });
                        return true;
                    }
                }
                else
                    return false;
            case 2: // 쉴드 배터리
                if (canUse)
                {
                    if (isCardAboutToUse)
                    {
                        UseCard(itemCard, () =>
                        {
                            parentShip.ActivateShieldBattery(true);
                        });
                        parentShip.UsePower(shieldData.powerUsage);
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            default:
                return false;
        }
    }
}
