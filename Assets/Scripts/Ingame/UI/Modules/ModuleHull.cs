﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZF;

public class ModuleHull : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;

    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Hull)
                  .SetParentModule(this);

        for (int i = 0; i < DataManager.hullData.dataList.Count; i++)
        {
            cardDeck.RegisterCard((card, visible) =>
            {
                var hullData = DataManager.GetHullData(i);
                card.SetCardIndex(i)
                    .SetNameText(hullData.name)
                    .SetTargetRequire(hullData.targetRequire)
                    .SetDescriptionText(hullData.description)
                    .SetPowerUsageText(hullData.powerUsage)
                    .SetTypeImage(ModuleType.Hull)
                    .SetIconImage(AssetManager.instance.GetHullSprite(i))
                    .SetActionPointerDown(SetActionPointerDown)
                    .SetActionEndDrag(EndDrag)
                    .SetInteraction(visible);
            });
        }
    }

    public override bool EndDrag(UIItemCard itemCard)
    {
        var hullData = DataManager.GetHullData(itemCard.cardIndex);
        return UseSpecialCard(itemCard, hullData);
    }

    public bool UseSpecialCard(UIItemCard itemCard, HullDataRow hullData)
    {
        bool canUse = parentShip.power - hullData.powerUsage >= 0;
        if (canUse)
        {
            switch (hullData.id)
            {
                case 1: // 모듈 수리
                    if (UIItemModule.selectedModule == null)
                        return false;
                    else
                    {
                        var selectedModule = UIItemModule.selectedModule;
                        if (selectedModule.hitpoints == 0) // 내구도 0 이면 수리 불가능
                            return false;

                        if (selectedModule.isMyModule &&
                            selectedModule.hitpoints < selectedModule.maxHitpoints)
                        {
                            UseCard(itemCard, () =>
                            {
                                selectedModule.GainHitpoints(2);
                            });
                            parentShip.UsePower(hullData.powerUsage);
                            return true;
                        }
                        else
                            return false;
                    }
                case 2: // 모듈 재구축
                    if (UIItemModule.selectedModule == null)
                        return false;
                    else
                    {
                        var selectedModule = UIItemModule.selectedModule;
                        if (selectedModule.hitpoints > 0) // 내구도가 0 초과면 구축 불가능
                            return false;

                        if (selectedModule.isMyModule)
                        {
                            UseCard(itemCard, () =>
                            {
                                selectedModule.SetHitpoints(selectedModule.maxHitpoints / 2);
                            });
                            parentShip.UsePower(hullData.powerUsage);
                            return true;
                        }
                        else
                            return false;
                    }
                case 3: // 과열
                    if (isCardAboutToUse)
                    {
                        UseCard(itemCard, () =>
                        {
                            parentShip.SetOverHeat(true);
                        });
                        parentShip.UsePower(hullData.powerUsage);
                        return true;
                    }
                    else
                        return false;
                default:
                    return false;
            }
        }
        else
            return false;
    }
}
