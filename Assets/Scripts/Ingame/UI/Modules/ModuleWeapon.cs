﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleWeapon : ModuleBase
{
    [SerializeField] UICardDeck cardDeck;
    [SerializeField] ProjectileController projectilController;
    [SerializeField] Transform[] launchableTransform;

    public void SetInfo()
    {
        itemModule.SetModuleTypeText(ModuleType.Weapon)
                  .SetParentModule(this);

        for (int i = 0; i < DataManager.weaponData.dataList.Count; i++)
        {
            for (int n = 0; n < 2; n++) 
                cardDeck.RegisterCard((card, visible) =>
                {
                    var weaponData = DataManager.GetWeaponData(i);
                    card.SetCardIndex(i)
                        .SetTargetRequire(weaponData.targetRequire)
                        .SetNameText(weaponData.name)
                        .SetDescriptionText(weaponData.description)
                        .SetPowerUsageText(weaponData.powerUsage)
                        .SetTypeImage(ModuleType.Weapon)
                        .SetIconImage(AssetManager.instance.GetWeaponSprite(i))
                        .SetActionPointerDown(SetActionPointerDown)
                        .SetActionEndDrag(EndDrag)
                        .SetInteraction(visible);
                });
        }
    }

    // 공격 모듈은 그냥 단순하게 공격만 하는 기능밖에없어서 일단 따로 아이디별 기능을 만들어 두지 않음
    public override bool EndDrag(UIItemCard itemCard)
    {
        var selectedModule = UIItemModule.selectedModule;
        if (selectedModule != null)
        {
            // 공격 대상이 내 모듈이면 아무 것도 하지 않는다.
            if (selectedModule.isMyModule)
                return false;            
            else if (selectedModule.hitpoints <= 0)
                return false;
            else
            {
                var weaponData = DataManager.GetWeaponData(itemCard.cardIndex);
                var powerUsage = parentShip.isOverHeat ? weaponData.powerUsage * 2 : weaponData.powerUsage;
                bool canUse = parentShip.power - powerUsage >= 0;

                if (canUse)
                {
                    UseCard(itemCard, () =>
                    {
                        // 상대 모듈에 데미지 주고 내 동력 사용량 감소 시킴
                        parentShip.UsePower(powerUsage);

                        var startPos = launchableTransform[Random.Range(0, launchableTransform.Length)].position;
                        var endPos = selectedModule.modulePosition;
                        var sprite = AssetManager.instance.GetProjectileSprite(weaponData.id - 1);

                        projectilController.FireProjectile(startPos, endPos, sprite,
                            weaponData.id == 1 ? new Color32(0x9F, 0x27, 0x20, 0xFF) : new Color32(0x3B, 0xA1, 0x00, 0xFF),
                            weaponData.id == 1 ? new Color32(0xC3, 0x00, 0x99, 0xFF) : new Color32(0x00, 0xC3, 0xBA, 0xFF),
                            () =>
                            {
                                selectedModule.TakeDamageByWeapon(parentShip.isOverHeat ? weaponData.damage * 2 : weaponData.damage);
                                CameraShaker.instance.ShakeCamera();
                            });
                    });
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        else
            return false;
    }
}
