﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDPower : MonoBehaviour
{
    [SerializeField] List<GameObject> goPowerList;
    [SerializeField] Text txtPower;

    public void SetPower(int currentPower, int maxPower, int maxLimitPower)
    {
        txtPower.text = $"{currentPower}/{maxPower}";
        for(int i = 0; i < maxLimitPower; i++)
        {
            goPowerList[i].SetActive(i < currentPower);
        }
    }
}
