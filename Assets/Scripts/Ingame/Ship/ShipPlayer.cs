﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShipPlayer : ShipBase
{
    // Start is called before the first frame update
    void Start()
    {
        InitShip();
    }

    public void ShowCards(Action callback)
    {
        cardDeck.ShowCards(callback);
    }

    public void ClearCards()
    {
        cardDeck.ClearCards();
    }
}
