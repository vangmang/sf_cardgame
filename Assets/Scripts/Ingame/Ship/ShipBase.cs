﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipBase : MonoBehaviour
{
    [SerializeField] protected ModuleWeapon moduleWeapon;
    [SerializeField] protected ModuleShield moduleShield;
    [SerializeField] protected ModuleEngine moduleEngine;
    [SerializeField] protected ModuleHull moduleHull;

    [SerializeField] GameObject goShieldBattery;
    [SerializeField] GameObject goOverheat;

    public HUDShield hudShield;
    public HUDPower hudPower;
    public UICardDeck cardDeck;

    public readonly int maxLimitPower = 10;
    public int maxHitpoints { get; private set; }
    public int maxPower { get; private set; }
    public int maxShield { get; private set; }

    public int hitpoints;
    public int power;
    public int shield;

    public bool isShieldBatteryEnable { get; private set; }
    public int shieldBatteryDuration { get; private set; }
    public bool isOverload { get; private set; } // 과부하
    public bool isOverHeat { get; private set; } // 과열
    public void RecoveryShield(int amount)
    {
        shield += amount;
        shield = Mathf.Min(shield, maxShield);
        hudShield.SetShield(shield, maxShield);
    }

    public void RecoveryPower(int amount)
    {
        power += amount;
        power = Mathf.Min(power, maxPower);
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public void TakeDamage(int amount)
    {
        hitpoints -= amount;
        hitpoints = Mathf.Max(hitpoints, 0);
        if(hitpoints <= 0)
        {
            //DEAD
        }
    }

    public int ReduceShield(int amount)
    {
        int overTakenDamage = shield - amount;
        shield -= amount;
        shield = Mathf.Max(shield, 0);
        hudShield.SetShield(shield, maxShield);
        return overTakenDamage;
    }

    public void UsePower(int usageAmount)
    {
        power -= usageAmount;
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public void InitShip()
    {
        moduleWeapon.SetMaxHitpoints(20);
        moduleShield.SetMaxHitpoints(10);
        moduleEngine.SetMaxHitpoints(25);
        moduleHull.SetMaxHitpoints(50);

        moduleWeapon.SetInfo();
        moduleShield.SetInfo();
        moduleEngine.SetInfo();
        moduleHull.SetInfo();

        maxHitpoints = 30;
        hitpoints = maxHitpoints;
        maxPower = 5;
        power = maxPower;
        maxShield = 10;
        shield = maxShield;

        hudShield.SetShield(shield, maxShield);
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    public void ActivateShieldBattery(bool enable)
    {
        isShieldBatteryEnable = enable;
        if (enable)
            shieldBatteryDuration = 3;
        goShieldBattery.SetActive(enable);
    }

    public void ReduceShieldBatteryDuration()
    {
        shieldBatteryDuration--;
        shieldBatteryDuration = Mathf.Max(shieldBatteryDuration, 0);
    }

    public void SetOverload(bool enable)
    {
        isOverload = enable;
    }

    public void SetOverHeat(bool enable)
    {
        isOverHeat = enable;
        goOverheat.SetActive(enable);
    }

    // 턴이 종료되고 정보 업데이트
    public virtual void UpdateInfo()
    {
        UpdatePower();
        UpdateShield();
        UpdateWeapon();
    }

    protected void UpdatePower()
    {
        if (moduleEngine.hitpoints > 0)
        {
            maxPower++;
            maxPower = Mathf.Min(maxLimitPower, maxPower);
            power = maxPower;
            if(isOverload)
            {
                power /= 2;
                SetOverload(false);
            }
        }
        else
        {
            maxPower = 2;
            power = maxPower;
        }
        hudPower.SetPower(power, maxPower, maxLimitPower);
    }

    protected void UpdateShield()
    {
        shield = moduleShield.hitpoints;
        hudShield.SetShield(shield, maxShield);
        ReduceShieldBatteryDuration();
        if (shieldBatteryDuration == 0)
            ActivateShieldBattery(false);
    }

    protected void UpdateWeapon()
    {
        if(isOverHeat)
        {
            SetOverHeat(false);
        }
    }
}
