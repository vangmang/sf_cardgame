﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEnemy : ShipBase
{
    [SerializeField] ShipPlayer player;

    // Start is called before the first frame update
    void Start()
    {
        InitShip();
    }

    Coroutine coroutine;
    public void PlayAI()
    {
        UpdateInfo();
        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = StartCoroutine(CoPlayAI());
    }

    private IEnumerator CoPlayAI()
    {
        yield return new WaitForSeconds(2f);

        GameManager.instance.ToMyTurn();
    }
}
