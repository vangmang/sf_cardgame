﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix;

public class AssetManager : MonoBehaviour
{
    public static AssetManager instance { get; private set; }
    
    void Awake()
    {
        instance = this;
    }

    [SerializeField] List<Sprite> typeSprites;
    [SerializeField] List<Sprite> weaponSprites;
    [SerializeField] List<Sprite> shieldSprites;
    [SerializeField] List<Sprite> engineSprites;
    [SerializeField] List<Sprite> hullSprites;
    [SerializeField] List<Sprite> projectileSprites;

    public Sprite GetProjectileSprite(int key)
    {
        return projectileSprites[key];
    }

    public Sprite GetTypeSprite(ModuleType moduleType)
    {
        return typeSprites[(int)moduleType];
    }

    public Sprite GetWeaponSprite(int key)
    {
        return weaponSprites[key];
    }

    public Sprite GetShieldSprite(int key)
    {
        return shieldSprites[key];
    }

    public Sprite GetEngineSprite(int key)
    {
        return engineSprites[key];
    }

    public Sprite GetHullSprite(int key)
    {
        return hullSprites[key];
    }
}
